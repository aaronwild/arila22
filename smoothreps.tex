\chapter{Galois representations}

\section{Generalities}

\begin{numtext}
  Let $K'/K$ be a normal and separable extension of fields.
  Recall that we have the associated Galois group,
  \[
    \gal(K'/K) 
    = 
    \Set{\sigma \in \Aut(K') \given \restrict{\sigma}{K} = \id_K}.
  \]
  It has a natural topology, with a basis of open neighborhoods given by 
  \[
    \Set{\gal(K'/L) \given K \sse L \text{ is a finite Galois extension}}.
  \]
  Equivalently, we have 
  \[
    \gal(K'/K)
    = 
    \varprojlim_{K \sse L\text{ is finite-Galois}} 
    \gal(L/K).
  \]
  So as a topological space, $\gal(K'/K)$ is a profinite group. 
  \par 
  The fundamental theorem of Galois theory for finite extensions extends to the infinite extensions as follows (were we write $\gal(K'/K) = G$ for brevity):
  \begin{inthm}
  \begin{enumerate}
    \item 
      The closed subgroups of $G$ are given by the collection of $\gal(K'/L)$ for $K \sse L \sse K'$ the intermediate extensions.
    \item 
      The open subgroups of $G$ are given by the collection of $\gal(K'/L)$ for $K \sse L \sse K'$ with $K \sse L$ finite.
    \item 
      A closed subgroup $H \sse G$ is normal if and only if $K \sse K'^H$ is a Galois extension.
    \item 
      An open subgroup $H \sse G$ is normal if and only if $K \sse K'^H$ is a finite Galois extension.
  \end{enumerate}
\end{inthm}
\end{numtext}
We are now interested in the representation theory of these Galois groups, and specifically, of the \emph{absolute Galois group}, $G_K \defo \gal(K^{\mathrm{sep}}/K)$.

\begin{defn}
  Let $L$ be a topological field.
  A $n$-dimensional \emph{Galois representation} of $K$ with values in $L$ is a continous group homomorphism 
  \[
    \rho_K\mc G_K \to \GL_n(L).
  \]
\end{defn}

\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item 
      If $L$ is a discrete field, then $\rho$ factors over a finite subgroup $\gal(L/K)$ for some finite extension $K \sse L$, and moreover, $\im(\rho) \sse \GL_n(L)$ is finite.
    \item 
      \label{lem:item:complex-galois-rep}
      If $L = \cc$ with the euclidean topology, then $\rho$ factors through some finite extension, $\im(\rho)$ is finite and conjugate to a representation valued in $\GL_n(\ov{\rat})$.
  \end{enumerate}
\end{lem}

\begin{proof}
  We only prove part \ref{lem:item:complex-galois-rep}.
  Note that the topology on $G_{K}$ allows for arbitrary small subgroups, in the sense that every neighborhood of the identity contains a non-trivial subgroup. 
  However, the same is not true for $\GL_n(\cc)$:
  \begin{fact}
    There is an open neighborhood $V$ of the identity matrix $\mathbf{1}_n \in \GL_n(\cc)$ that does not contain any non-trivial subgroup.
  \end{fact}
  Let now $\rho \mc G_K \to \GL_n(\cc)$ be a Galois representation.
  Then for $V$ as in the fact, we have that $\rho^{-1}(V)$ is an open neighborhood of the identity, and so (by construction of the basis of the topology on $G_K$), contains some open normal subgroup $\gal(\ov{K}/L)$ with $K \sse L$ a finite Galois extension.
  As $\rho(\gal(\ov{K}/L)) \sse V$, we get that $\rho(\gal(\ov{K}/L)) = \Set{\mathbf{1}_n}$.
  Moreover, since $\gal(\ov{K}/L)$ is a normal subgroup, we have $G_K / \gal(\ov{K}/L) = \gal(L/K)$, and $\rho$ factors over this quotient.
  \par 
  To show that $\rho$ is conjugate to a representation valued in $\GL_n(\ov{\rat})$, we consider the trace field 
  \[
    \rat_{\mathrm{tr}} \defo \rat(\Set{\chi(g)\given g \in \gal(L/K)})
  \]
  of the induced representation of $\gal(L/K)$, where 
  \[
    \chi \mc \gal(L/K) \to \cc,~ g \mapsto \tr(\rho(g))
  \]
  denotes the character of the representation.
  The extension $\rat_{\mathrm{tr}} / \rat$ is finite and algebraic, as each of the $\chi(g)$ is an algebraic integer.
  \begin{fact}
    Let $G$ be a finite group and $E$ a field of characteristic zero.
    Then every finite-dimensional representation of $G$ over $E$ is uniquely determined by its character.
  \end{fact}
\end{proof}

\begin{example}
  For $n \geq 1$, denote by $\mu_n \sse \ov{\rat}$ the group of $n$-th roots of unity.
  This is a cyclic group of order $n$, and generator $\zeta_n$ is called a \emph{primitive} $n$-th root of unity.
  Fix such a $\zeta_n$ for now.
  Recall that after this choice, we get an isomorphism 
  \[
    \gal(\rat(\zeta_n)/\rat) 
    \isomorphism 
    (\zz/n\zz)^\times,
  \]
  which is uniquely determined $\sigma \mapsto a$ for $\sigma(\zeta_n) = \zeta_n^a$.
  Fix now a prime $p$. 
  For the field obtained by adjoining all possible $p^n$-th roots of unity, that is, 
  \[
    \rat(\zeta_{p^\infty}) 
    \defo
    \bigcup_{n\geq 1} 
    \rat(\zeta_{p^n}),
  \]
  we have $\gal(\rat(\zeta_{p^\infty})/\rat) = \varprojlim_n \gal(\rat(\zeta_{p^n}/\rat))$, and the transition maps are compatible with the above isomorphisms.
  In other words, we obtain an isomorphism 
  \[
    \gal(\rat(\zeta_{p^\infty})/\rat)
    \isomorphism 
    \zz_p^\times.
  \]
  Composing this on the right with the embedding $\zz_p^\times \subseteq \rat_p^\times$ and on the left with the projection $G_{\rat} \twoheadrightarrow \gal(\rat(\zeta_{p^\infty})/\rat )$ yields a continous one-dimensional Galois representation 
  \[
    G_{\rat} 
    \longrightarrow 
    \GL_1(\rat_p)
  \]
  with infinite image.
  This representation is called the \emph{$p$-adic cyclotomic character} of $\rat$.
\end{example}


\section{Local representations when $\ell \neq p$}
\begin{setup}
  In this section, we consider $p$-adic representations of an $\ell$-adic local field, under the assumption $p \neq \ell$.
  To fix notation, $K / \ql$ denotes a local field, with ring of integers $\oo_K \sse K$.
  Recall that $\oo_K$ is a discrete valuation ring, where the valuation is induced by the $\ell$-adic valuation $\nu_K \mc K^\times \to \zz$.
  As such, its maximal ideal $\idm_K$ is generated by a single element, a(ny) choice of which is called a \emph{uniformizer} of $K$, and which we will denote by $\upi_K$.
  Finally, we will write $k \defo k(K)$ for the resiude field of $\oo_K$, that is, $k = \oo_K / \idm_K$, and $q$ for its (finite) cardinality.
\end{setup}
\begin{numtext}
  Let $K\ur$ be the maximal unramified extension of $K$, that is, 
  \[
    K\ur = \bigcup_{\substack{K \sse K' \\ \text{finite, unramified}}} K' = \bigcup_{n\geq 1} K(\zeta_{q^n-1}).
  \]
  Recall that we have a canonical isomorphism $\gal(K\ur / K) \isomorphism G_k$, and thus a canonical surjection $G_K \twoheadrightarrow G_k$.
  We denote its kernel by $I_K$ and call it the \emph{inertia subgroup} of $G_K$. 
  Let $\frob_K = \frob_k \in G_k$ be the geometric Frobenius element, i.e. the automorphism of $\ov{k}$ given by $x \mapsto x^q$.
  It is a \emph{topological generator} of $G_k$, in the sense that the subgroup $\frob_k^\zz \sse G_k$ is dense (in other words, $\frob_k \in G_k$ is induced by the collection of Frobenius on the finite extensions of $k$).
  Denote by $W_K$ the preimage of $\frob_k^\zz$ under the surjection $G_K \twoheadrightarrow G_k$; so we have the commutative diagram of abstract groups 
  \[
    \begin{tikzcd}
      0 
      \ar{r}
      &
      I_K
      \ar[hookrightarrow]{r}
      \ar[equal]{d}
      &
      G_K 
      \ar{r} 
      &
      G_k 
      \ar{r}
      & 
      0 
      \\
      0 
      \ar{r} 
      & 
      I_K 
      \ar{r}
      & 
      W_K 
      \ar{r}
      \ar[hookrightarrow]{u}
      &
      \frob_k^\zz 
      \ar[hookrightarrow]{u}
      \ar{r} 
      &
      0 
    \end{tikzcd}
  \]
  However, the topology of $W_K$ that we want is not the subspace topology, but rather induced by requiring that $I_K$ is open and continues to carry its natural topology via the description $I_K = \gal(\ov{K}/K\ur) \sse G_K$.
  Local class field theory relates the continous irreducible representations of 
  \[
    W_K^\ab \defo W_K / \ov{\left[W_K,W_K\right]}
  \]
  to the representations of $K^\times = \GL_1(K)$ in the following sense:
\end{numtext}
\begin{theorem}
  For every finite extension $K / \ql$, there is a unique isomorphism 
  \[
    \art_K \mc K^\times \isomorphism W_K^\ab
  \]
  of topological groups such that:
  \begin{enumerate}
    \item 
      If $K'/K$ is a finite extennsion, then $\art_{K'} = \art_K \circ N_{K'/K}$.
    \item 
      The diagram 
      \[
        \begin{tikzcd}
          K^\times 
          \ar{r}[above]{\art_K}
          \ar{d}{\nu_K}
          &
          \ar[epi]{d}
          \\
          \zz 
          \ar{r}
          &
          \frob_K^\zz 
        \end{tikzcd}
      \]
      in which the bottom arrow is given by $a \mapsto \frob^a_K$, commutes.
  \end{enumerate}
\end{theorem}
Note that continous irreducible representations of $W_K^\ab$ are the same as continous characters of $W_K$.
Our next goal is to relate a certain class of finite dimensional representations of $W_K$ to continous representations of $G_K$.
For that, we need to make a short digression into local class field theory:

\subsection*{Inertia and higher ramification groups}

\begin{defn}
  Let $K'/K$ be a finite extension.
  Set 
  \[
    I_{K'/K}
    \defo 
    \Set{\sigma \in \gal(K'/K) \given \restrict{\sigma}{k'} = \id_{k'}},
  \]
  which is called the \emph{intertia group} of $K'/K$.
\end{defn}

\begin{rem}
  Note that $K'/K$ is unramified if and only if $I_{K'/K} = \Set{\id}$.
\end{rem}

\begin{defn}
  More generally, for $i \geq -1$, we set 
  \[
    G_i \defo 
    \Set{\sigma \in \gal(K'/K) \given \restrict{\sigma}{\oo_{K'}/\idm_{K'}^{i+1}} = \id_{\oo_{K'}/\idm_{K'}^{i+1}}},
  \]
  which is called the \emph{$i$-th ramification group} of $K'/K$.
  Moreover, the group 
  \[
    P_{K'/K} \defo G_1 
  \]
  is called the \emph{wild inertia group} of $K'/K$, and 
  \[
    I\tame_{K'/K} \defo I_{K'/K} / P_{K'/K}
  \]
  is called the \emph{tame inertia group} of $K'/K$.
\end{defn}

\begin{lem}
  Let $K'/K$ be a finite Galois extension, with ramification index $e$.
  \begin{enumerate}
    \item 
      $\abs{I_{K'/K}} = e$.
    \item 
      $P_{K'/K} \sse I_{K'/K}$ is finite, and in fact a pro-$\ell$ group. 
    \item 
      $I_{K'/K}\tame$ has order the prime-to-$\ell$-part of $e$.
    \item 
      There is a canonical isomorphism 
      \[
        \theta 
        \mc 
        I_{K'/K}\tame \isomorphism \mu_e(k').
      \]
  \end{enumerate}
\end{lem}

\begin{proof}
  We prove this in three steps:
  \begin{proofstep}
    There is a well-defined group homomorphism 
    \[
      \theta
      \mc 
      I_{K'/K}^t 
      \to \mu_e(k'),
    \]
    that does not depend on the choice of a pseudo-uniformizer of $\oo_{K'}$.
  \end{proofstep}
  \begin{proofstep}
    For $\theta$ as above, we have 
    $ 
      \ker \theta = P_{K'/K}.
    $ 
  \end{proofstep}
  \begin{proofstep}
    $P_{K'/K}$ is an $\ell$-group.
  \end{proofstep}
\end{proof}

\subsection*{Weil-Deligne Representations}

\begin{defn}
  A \emph{Weil-Deligne representation} is a pair $(r,N)$, where 
  \[
    r\mc W_K \to \GL(V)
  \]
  is a finite dimensional representation of $W_K$ on a finite dimensional $L$-representation, and $N$ is a nilpotent endomorphism of $V$, such that 
  \[
    r(\sigma) N r(\sigma)^{-1} = q^{-\nu_K(\art^{-1}(\sigma))}
  \]
  holds for all $\sigma \in W_K$.
\end{defn}

\begin{lem}
  \label{lem:algebraic-conjugate-to-integral}
  Let $L/\qp$ be an algebraic extension, and $\rho\mc G_K \to \GL_n(L)$ a Galois representation.
  Then there exists an $\oo_L$-lattice $\Lambda \sse L^n$ such that $\rho$ stabilizes $\Lambda$.
  In particular, $\rho$ is conjugate to a Galois representation $\rho'\mc G_K \to \GL_n(\oo_L)$.
\end{lem}

\begin{proof}
  If $\Lambda \sse L^n$ is an $\oo_L$-lattice, then its stabilizer in $\GL_n(L)$ is conjugate to $\GL_n(\oo_L)$.
\end{proof}

\begin{defn}
  Let $L/\qp$ be an algebraic extension.
  A matrix $A\in \GL_n(L)$ is \emph{bounded} if $\det A \in \oo_L^\times$ and the characteristic polynomial of $A$ has coefficients in $\oo_L$.
\end{defn}
\begin{lem}
  \label{lem:bounded-equiv-cond}
  Let $L/\qp$ be an algebraic extension.
  For $A \in \GL_n(L)$, the following are equivalent:
  \begin{enumerate}
    \item 
      $A$ is bounded.
    \item 
      $A$ stabilizes an $\oo_L$-lattice $\Lambda$, i.e. $\restrict{A}{\Lambda}$ is an automorphism.
    \item 
      $A$ is conjugate to a matrix in $\GL_n(\oo_L)$.
  \end{enumerate}
\end{lem}

\begin{theorem}
  Suppose that $K/\ql$ is a finite extension.
  Let $V$ be a finite-dimensional $L$-vector space, for $L$ an algebraic extension of $\qp$.
  Fix $\pphi \in W_K$ a lift of $\frob_K$ and a compatible system $(\zeta_m)_{(m,l) = 1}$ of primitive roots of unity.
  Let $\rho \mc G_K \to \GL(V)$ be a continous Galois representation.
  \begin{enumerate}
    \item
      \label{thm:grod-monodromy:nilpotent}
      There is a finite extension $K'/K$ and a uniquely determind nilpotent $N \in \End(V)$ such that 
      \[
        \rho(\sigma) = \exp(N t_{\zeta,p}(\sigma))
      \]
      holds for all $\sigma \in I_{K'}$.
    \item 
      \label{thm:grod-monodromy:equivalence}
      Setting 
      \[
        r(\rho) 
        \defo 
        \rho(\sigma) \exp(-t_{\zeta,p}(-\pphi^{-\nu(\sigma)} \sigma)N)
      \]
      defines a Weil-Deligne representation.
      This assignement yields an equivalence of categories
      \[
        \Set{\text{Galois representations of $G_K$}}
        \isomorphism 
        \Set{\text{bounded WD-representations of $W_K$}}
        .
      \]
    \end{enumerate}
\end{theorem}

\begin{proof}
  We show the analogous version of \ref{thm:grod-monodromy:nilpotent} for $G_K$ replaced with $W_K$.
  After the choice of a basis of $V$ we might assume that we have 
  \[
    \rho \mc W_K \to \GL_n(L).
  \]
  Our goal is to produce an open subgroup $N \sse I_K$ such that $\restrict{\rho}{N}$ has image in the unipotent matrices.
  To that end, consider the (for the moment seemingly randomly defined) subgroups 
  \[ 
    K_m \defo 1 + p^m \Mat_n(\oo_L) \subset \GL_n(L).
  \]
  These are open subgroups,$K_{m+1} \subset K_m$ is normal and $K_m/K_{m+1}$ has order $p$.
  With these at hand, we set 
  \[
    J \defo \Set{g \in \ker(t\mc I_K \to \zz_p) \given \rho(g) \in K_2}.
  \]
  Note that $t = t_{\zeta,p}$ depends on the choice of some root of unity, but we will later show that the isomorphism class of the induced Weil-Deligne representation does not depend on $\zeta$.
  Now by continuity of $\rho$, we have that $J \sse \ker(t)$ is open; moreover, $\sigma(J) \in K_2/K_3$ has order $p$ or is trivial.
  Since $J$ has pro-order prime to $p$ (as it is an open subgroup of $\ker(t)$), $\sigma(J) \in K_3$ follows, and inductively, $\rho(J) \sse K_m$ follows for all $m \geq 2$.
  By passing to the limit, we obtain that $\rho(J) \subset \GL_n(L)$ is in fact trivial.
  \par 
  Moving the different subspace topologies around, we can thus find an open normal subgroup $H \sse W_K$ such that
  \[
    H \cap I_K \sse J \sse \ker(t)
  \]
  with $\restrict{\rho}{H \cap I_K}$ being trivial.
  In other words, we obtain a factorization in the diagram 
  \[
    \begin{tikzcd}
      H \cap I_K 
      \ar{d}
      \ar{r}[above]{\rho}
      &
      \GL_n(L)
      \\
      t(H \cap I_K) 
      \ar[dashed]{ur}
      &
    \end{tikzcd}
  \]
  By the construction of $t$, we then have 
  \begin{equation}
    \label{proof:conjugation-for-inertia}
    \tag{$\ast$}
    \rho(\phi x \phi^{-1})^{m} = \rho(h)
  \end{equation}
  for all $x \in H \cap I_K$ (here, $m$ is the cardinality of the residue field of $K$).
  Consider now 
  \[
    X = \log(\rho(x)) \defo \sum_{j \geq 1} (-1)^{j-1} \frac{(\rho(x)-1)^j}{j},
  \]
  which converges since we've observed that 
  \[
    \frac{(\rho(x) - 1)^j}{j} \equiv 0 \mod p^j 
  \]
  holds for all $j \geq 2$.
  By \eqref{proof:conjugation-for-inertia}, we have that $X$ and $qX$ are conjugate\footnote{... up to me showing that $\log(B) \sim \log(ABA^{-1})$ holds(?) for invertible matrices}.
  In particular, all the coefficients of their characteristic polynomials agree.
  If we write $a_i$ for the $(n-i)$-th coefficient of $\chi_X$, we thus have 
  \[
    a_i = q^i a_i 
  \]
  for all $0 \leq i \leq n$.
  So each of the $a_i$ is zero, and thus $X$ is nilpotent, which implies that 
  \[
    \rho(x) = \exp(\log(x)) = \exp(X)
  \]
  is unipotent.
  \par 
  With this subgroup at hand, we can now show the statement of \ref{thm:grod-monodromy:nilpotent} for $W_K$:
  For any $x\in H \cap I_K$ with $t(x) \neq 0$, we have that 
  \[
    N \defo t(x)^{-1} \log(\rho(x)) \in \Mat_n(L)
  \]
  is nilpotent, and moreover, that the maps 
  \[
    x \mapsto \rho(x) ~~\text{and}~~ x \mapsto \exp(xN)
  \]
  agree on the closure of $x \zz_p$.
  So they also agree on $t^{-1}(\ov{x \zz_p}) \sse I_F$, which is open (note that the closure of $x\zz_p$ in  $\zz_p$ is in fact also open).
  \par 
  For now, we omit extending this proof to all of $G_K$, and also don't show that the functor so defined is actually an equivalence of categories.
\end{proof}

\section{Semisimplicity}

\begin{defn}
  Let $V_K$ be a continous $G_K$ representation.
  We say that $V_K$ is \emph{$\pphi_K$-semisimple} if $\pphi_K$ acts semisimple on all subspaces $V^{I'}$ with $I'$ an open subgroup of the intertia group $I_K$.
\end{defn}
\begin{lem}
  \label{lem:semisimple-on-closure}
  Let $A,B$ be rings (not necessarily commutative), and $A \to B$ a ring map such that $B$ becomes a free $A$-algebra.
  Let $M$ be an $A$-module such that $B \tensor_A M$ is a semisimple $B$-module.
  Then $M$ is a semisimple $A$-module.
\end{lem}
\section{$\ell$-adic representations arising from geometry}
\subsection*{Tate modules of elliptic curves}
\begin{litnote}
  \cite{campo-elliptic}.
\end{litnote}

\begin{defn}
  Let $F$ be a field of characteristic $0$ or $p$, and let $A$ be an abelian group.
  Fix a prime $\ell \neq p$.
  Then the \emph{Tate module} of $A$ is defined as 
  \[
    T_{\ell}A 
    \defo 
    \varprojlim
    (
    \begin{tikzcd}[column sep = small]
      {A[\ell]}
      &
      {A[\ell^ 2]}
      \ar{l}[above]{\cdot \ell}
      &
      {A[\ell^3]}
      \ar{l}[above]{\cdot \ell}
      &
      \ldots 
      \ar{l}
    \end{tikzcd}
    ).
  \]
  This is naturally a $\zl$-module and we write 
  \[
    V_{\ell}A 
    \defo 
    T_{\ell}A \tensor_{\zl} \ql 
  \]
  for its base change to $\ql$.
\end{defn}
\begin{defn}
  Let $F$ be a field of characteristic 0 or $p$, and $E/F$ an elliptic curve.
  Its \emph{Tate module} is defined as $T_{\ell}E \defo T_{\ell}E(\ov{F})$.
\end{defn}
\begin{numtext}
  As ``abstract module'', the Tate module is very explicit: we have 
  \[
    T_{\ell} E \cong \zl \oplus \zl 
  \]
  as $\zl$-module, and consequently $\Vl E = \ql \oplus \ql$.
  However, both $\Tl E$ and $\Vl E$ are in fact a Galois-representation, since each of the torsion points $E[\ell^n]$ has a continous $G_F$-action, that are compatible with the multiplication map.
  The main reason why we care about the Tate module is that it encodes the first \'{e}tale cohomology of the elliptic curve:
  \begin{inthm}
    Let $E/F$ be an elliptic curve over a field $F$, and $\ell \neq \rchar(\ov{F})$ a prime.
    Then there is a canonical $G_F$-invariant isomorphism 
    \[
      \het[1](E \times_F \ov{F},\zl) 
      \isomorphism 
      \hom_{\zl}(\Tl E, \zl).
    \]
  \end{inthm}
  We want to explicitly calculate the Galois representation $\Vl E$ for $E/K$ an elliptic curve over a local field $K$ in a simple instance.
  By simple, we mean that we assume the elliptic curve to have \emph{good reduction}: for the minimal Weierstrass model of $E$ with reduction $\snake{E}$ over $k$, we assume that $\snake{E}$ has no singularities (where $k$ is the residue field of $F$).
  \begin{inexample*}
    Let $K = \qp$ with $p >3$.
    Then 
    \[
      E_1 \mc y^ 2 = x^3 - x 
    \]
    has good reduction.
  \end{inexample*}
\end{numtext}

\begin{theorem}
  Let $K/\qp$ be a finite extension, and $E/K$ an elliptic curve with good reduction.
  Then $\Vl E$ is a two-dimensional $\pphi_K$-semisimple continous $G_K$ representation.
  Moreover, $\Vl E$ is unramified\footnote{i.e. $I_K$ acts trivially} and $\frob_K$ has characteristic polynomial 
  \[
    T^2 - aT + q ~~ \text{where} ~~ a = q+1 - \abs{\snake{E}(k)}.
  \]
  In particular, over $K' \defo \ql(\sqrt{a^2 - 4q})$, the representation $\Vl E$ splits as sum of two characters.
\end{theorem}

\begin{proof}
  The main point is that in case of good reduction, all the information of $E$ is essentially contained in $\snake{E}$, where explicit calculations can be made. 
  \begin{fact}%[\cite[VII. 3.1]{silverman-aec}]
    Let $E/K$ be an elliptic curve with good reduction.
    Then the reduction map $E \to \snake{E}$ induces an isomorphism 
    \[
      \Tl E \isomorphism \Tl \snake{E}.
    \]
  \end{fact}
  \begin{claim}
    Let $E$ be an elliptic curve over a field $L = \ff_q$ of finite characteristic $p$.
    Then the Frobenius automorphism $\frob_L$ acts semisimple on $\Vl E$ for $\ell \neq p$.
  \end{claim}
  \begin{proof*}
    The idea is that $\frob_L$ is a degree $q$ isogeny, and that for any isogeny, its degree yields a positive-definite quadratic form on $\End(E)$ (\cite[III.Cor.6.3]{silverman-aec}).
    So $\cc[\frob_L]$ is semisimple, and after choosing an (abstract) isomorphism $\cc \cong \ov{\rat}_p$, we obtain that $\Vl E \tensor \ov{\rat}_p$ is a semisimple $\ov{\rat}_p[\frob_L]$-module, and thus the same is true for $\Vl E$ (by \cref{lem:semisimple-on-closure}).
  \end{proof*}
  Combining the above two statements gives that $\Vl E$ is a semisimple $\frob_K$-module, as claimed.
  Moreover, since $\Vl E \cong \Vl \snake{E}$, we have necessarily that $\Vl E$ is an unramified representation.
  To get the explicit describtion of the characteristic polynomial of $\frob_K$ as an automorphism of $\Vl E$, we note that this agrees with the characteristic polynomial of $\frob_k \mc \snake{E} \to \snake{E}$ (this should follow from \cite[III.7.4]{silverman-aec}, the light-version of Falting's isogeny theorem(?)), which is explicitly calculated in \cite[V.Thm.2.3.1]{silverman-aec}.
\end{proof}


\subsection*{Modular curves}

\section{Local representations when $\ell = p$}
\begin{litnote}
  \cite{hodge-theory-gulotta}.
\end{litnote}
\begin{numtext}
  We now want to understand Galois representations $\rho\mc G_K \to \GL_n(L)$, where both $K$ and $L$ are extensions of $\qp$ with $K/\qp$ finite and $L/\qp$ algebraic.
  The crux is that many of them ``do not come from geometry'', in the sense that they are not given by the natural $G_K$ action on  
  $
    \het*(X_{\ov{K}},L)
  $ 
  for $X$ some smooth proper variety over $K$.
  \begin{inexample*}
    Let $\chi\mc G_{\qp} \to \qp^\times$ be an unramified character, such that $\chi(\frob_{\qp})$ is not an algebraic integer.
    Then $\chi$ does not arise from geometry.
  \end{inexample*}
\end{numtext}
In the following, we write $\cp$ for the $p$-adic completion of $\ov{\rat}_p$. 

\begin{theorem}[Hodge-Tate decomposition]
  Let $K / \qp$ be a finite extension, and $X/K$ a smooth proper variety.
  Then there is a $G_K$-invariant isomorphism 
  \[
    \het[n](X_{\ov{K}},\qp) \tensor_{\qp} \cp 
    \isomorphism 
    \bigoplus_{i+j = n}
    \hh[i](X, \Omega_{X/K}^ j) \tensor_K \cp(-j),
  \]
  where the $G_K$ action on the left-hand side is on both factors, and on the right-hand side, the $G_K$ action on $\hh[i](X,\Omega_{X/K}^j)$ is trivial and on $\cp(-j)$, it is the canonical one.
\end{theorem}
The proof is due to Faltings (\cite{faltings-hodge-tate}). 
Scholze gave a proof using perfectoid spaces in \cite{scholze-hodge-tate} (which in turn builds on ideas of Faltings).
%This proof is also explained in \cite{bhatt-hodge-tate}.

\begin{prop}[Tate]
  Let $K/\qp$ be a finite extension.
  Then  
  \[
    \hcont[0](G_K, \cp(i))
    =
    \begin{cases}
      0
      &
      \text{if $i \neq 0$,}
      \\
      K 
      &
      \text{if $i = 0$}
    \end{cases}
    ~~
    \text{and}
    ~~
    \hcont[1](G_K, \cp(i))
    =
    \begin{cases}
      0
      &
      \text{if $i \neq 0$,}
      \\
      K 
      &
      \text{if $i = 0$}
    \end{cases}
  \]
  In particular, if $i \neq j$, then 
  $
    \Hom_{G_K}(\cp(i),\cp(j))
    = 0.
  $
\end{prop}

\begin{cor}
  Let $X/K$ be a smooth proper variety.
  Then there are canonical isomorphisms 
  \[
    \left(\het[i+j](X_{\ov{K}}, \qp) \tensor_{\qp} \cp\right)^{G_K}
    =
    \hh[i](X,\Omega^{j}_{X/K}).
  \]
\end{cor}

\begin{defn}
  The \emph{Hodge-Tate period ring} is 
  \[
    \bht \defo \cp[t,t^{-1}],
  \]
  which caries a natural $G_K$-action via the cyclotomic character, i.e. we set 
  \[
    \sigma(at^i) \defo \sigma(a) \epsilon_p^i(\sigma)t^i,
  \]
  for $\sigma \in G_K$, $i \in \zz$ and $a\in \cp$.
\end{defn}

\begin{defn}
  Let $V$ be a finite-dimensional $\qp$-vector space, and $\rho \mc G_K \to \GL(V)$ a representation.
  We say that $\rho$ is a \emph{Hodge-Tate representation} or \emph{$\bht$-admissible} if 
  \[
    \dim_K(V \tensor_{\qp} \bht)^{G_K}
    = 
    \dim_{\qp} V 
  \]
  holds.
\end{defn}

\begin{theorem}[$p$-adic \'{e}tale to de-Rham comparison]
  There exists a ``period ring'' $\bdr$ over $K$ with a natural $G_K$-action and a filtration, such that for all smooth proper varieties $X/K$, there is a natural $G_K$-equivariant, filtration-preserving isomorphism 
  \[
    \het[n](X_{\ov{K}}, \qp)
    \tensor_{\qp}
    \bdr
    \isomorphism 
    \hdr[n](X/K)
    \tensor_K 
    \bdr.
  \]
\end{theorem}

\begin{defn}
  We say that a finite-dimensional representation $\rho\mc G_K \to \GL(V)$ is \emph{$\bdr$-admissible} or \emph{de Rham} if 
  \[
    \dim_K (V\tensor_{\qp} \bdr)^{G_K}
    = 
    \dim_{\qp} V 
  \]
  holds.
\end{defn}

\begin{numtext}
  There are two even finer requirements that can be imposed on a $p$-adic Galois representation, and all together, we have the following general hierarchy:
  \[
    \begin{tikzcd}
      \Set{\text{crystalline}}
      \ar[phantom]{r}[description]{\subsetneq}
      &
      \Set{\text{semi-stable}}
      \ar[phantom]{r}[description]{\subsetneq}
      &
      \Set{\text{de Rham}}
      \ar[phantom]{r}[description]{\subsetneq}
      &
      \Set{\text{Hodge-Tate}}
    \end{tikzcd}
  \]
  Moreover:
  \begin{enumerate}
    \item 
      In dimension one, the notion of a de Rham-representation and a Hodge-Tate-representation in fact do agree.
      The same is true for crystalline and semi-stable representations, these notions agree in dimension one (however, semi-stable and de Rham do not).
    \item 
      We want to think of crystalline representations as ``coming from a variety with good reduction'', and of semi-stable representations as ``coming from a variety with semi-stable reduction''.
  \end{enumerate}
\end{numtext}

There is the following $p$-adic analogue of Grothendiecks $\ell$-adic monodromy theorem:
\begin{theorem}
  Every de-Rham-representation $\rho\mc G_K \to \GL(V)$ is \emph{potentially semistable}, i.e. there is a finite extension $K'/K$ such that $\restrict{\rho}{G_{K'}}$ is semi-stable.
\end{theorem}
This was conjectured by Fontaine, and established by Berger in \cite{berger-monodromy}.
