# Lecture notes on the Arithmetic of the Langlands Program -- Nightly version

These are my lecture notes for the lecture [The arithmetic of the Langlands program][AC] by A. Cariani, held at the University of Bonn in the Winter Term 2022-23.
You are on the **nightly**-branch of these notes, meaning that everything you see here is subject to drastic changes and extremely error-prone.
Please keep this in mind!
If you are looking for a more polished version, please switch to the **main**-branch.
Of course, all errors are solely my fault.
For feedback, please get in touch via e-mail.
