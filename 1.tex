%\thispagestyle{plain}
\chapter{Introduction}
\section{Quadratic reciprocity}
The starting point of this introductory section is the law of quadratic reciprocity:
\begin{theorem}
  Let $p,\ell$ be distinct odd primes.
  Then the congruences 
  \begin{align*}
    x^2 - p & \equiv 0 \mod \ell 
    \\
    x^2 - \ell & \equiv 0 \mod p 
  \end{align*}
  are both solvable or both unsolvable, unless $p$ and $\ell$ are congruent $\mathrm{mod}~4$, in which case one of the congruences is solvable and the other one is not.
\end{theorem}
We will try to sketch a proof of this theorem, by relating characters of the \emph{absolute Galois group} $G_{\rat} \defo \gal(\ov{\rat} / \rat)$ to so-called Dirichlet characters.
\begin{numtext}
Consider the quadratic extension $\rat(\sqrt{p})/\rat$.
We will focus on the case in which $p$ is the only ramified prime of this extension (c.f. \cref{app:exm:ramified-quadratic}).
Recall that $\gal(\rat(\sqrt{p})/\rat) \cong \Set{\pm 1}$.
As $\rat(\sqrt{p}) \sse \ov{\rat}$, restricting gives a well-defined map 
\[
  \chi
  = 
  \restrict{(-)}{\rat(\sqrt{p})} 
  \mc 
  G_{\rat}
  \twoheadrightarrow 
  \gal(\rat(\sqrt{p})/\rat).
\]
Let $\frob_{\ell} \in \gal(\rat(\zeta_p)/\rat)$ be the Frobenius element (c.f.  for its construction).
We want to calculate $\chi(\frob)$ in two different ways, and then interpret the result as a form of quadratic reciprocity:
\begin{enumerate*}
  \item 
    We have the following equivalences:
    \[
      \chi(\frob_\ell) = 1 
      \Leftrightarrow 
      \frob_\ell(\sqrt{p}) = \sqrt{p}
      \Leftrightarrow 
      \sqrt{p} \in \ff_{\ell},
    \]
    i.e. $\chi(\frob_\ell) = 1$ if and only if $p$ is of quadratic residue modulo $\ell$.
  \item 
    By the Kronecker-Weber theorem\footnote{shorter: we know that $\rat(\zeta_p) / \rat$ is cyclic of order $p-1$, so contains a unique quadratic extension $\rat(\sqrt{d})$. Now $p$ is the only ramified prime of $\rat(\zeta_p)$, and in particular, it is also ramified in $\rat(\sqrt{d})$. Making use of \cref{app:exm:ramified-quadratic}, we conclude that $d = p$ (using our standing assumption on $p$).}, we have $\rat(\sqrt{p}) \subset \rat(\zeta_p)$.
    We are now in the following setup:
    \[
      \begin{tikzcd}
        \gal(\rat(\zeta_p)/\rat)
        \ar[iso]{r}
        \ar[twoheadrightarrow]{d}
        &
        (\zz/p\zz)^\times 
        \ar[twoheadrightarrow]{d}[right]{\chi'}
        \\
        \gal(\rat(\sqrt{p})/\rat)
        \ar[iso]{r}
        &
        \Set{\pm 1}
      \end{tikzcd}
    \]
    were we have restricted $\chi$ to $\gal(\rat(\zeta_p)/\rat)$. 
    Under the isomorphism $\gal(\rat(\zeta_p)/\rat)\cong (\zz / p\zz)^\times$, we have that $\frob_\ell$ gets mapped to $\ell \in (\zz/p\zz)^\times$.
    Moreover, we have 
    \[
      \chi'(\ell)
      =
      \begin{cases}
        1 
        &
        \text{if $\ell$ is an even power of the generator}
        \\
        -1 
        &
        \text{if $\ell$ is an odd power of the generator,}
      \end{cases}
    \]
    as this is in fact the only non-trivial quadratic character on $(\zz/p\zz)^\times$.
    So $\chi(\frob_{\ell}) = 1$ if and only if $\ell$ is of quadratic residue module $p$.
\end{enumerate*}
These two together imply quadratic reciprocity as in the theorem. 
\end{numtext}

\blindtext

\section{Galois representations and elliptic curves}
\begin{numtext}
Consider the equation 
\begin{equation}
  \tag{$\dagger$}
  \label{intro:eq:elliptic}
  x^3-x-1 \equiv 0 \mod \ell .
\end{equation}
We want to understand \emph{for what $\ell$ this splits into a product of three distinct factors?}
Let $K$ be the splitting field of the polynomial $f = x^3 - x- 1 \in \rat[x]$.
\begin{inlem}
  We have $\gal(K/\rat) \cong \mathfrak{S}_3$ and $K$ is a cubic extension of $\rat(\sqrt{-23})$.
  Moreover, we have that $K$ is the maximal unramified abelian extension of $\rat(\sqrt{-23})$.
\end{inlem}
\begin{proof*}
  We first recall a general fact:
  \begin{fact*}
    Let $f \in \rat[x]$ be a cubic polynomial, with splitting field $K_f$ and discriminant $\Delta_f$.
    If $\sqrt{\Delta_f} \in \rat$, then $\gal(K_f/\rat) \cong \mathfrak{A}_3$. Otherwise, $\gal(K_f/\rat) \cong \mathfrak{S}_3$.
  \end{fact*}
\end{proof*}

So $K$ is only ramified at $23$, which leads us to assuming $\ell \neq 23$ in what follows.
\end{numtext}

\begin{numtext}
Consider the permutation representation of $\Sf_3$.
This decomposes into the trivial representation and a $2$-dimensional irreducible representation, so we get 
\[
  \begin{tikzcd}[column sep = scriptsize]
    \gal(K/\rat)
    \ar[iso]{r}
    &
    \Sf_3 
    \ar[mono]{r}{\rho_K}
    &
    \GL_2(\rat).
  \end{tikzcd}
\]
Explicitly, for the presentation
\[
  \Sf_3 
  = 
  \genrel{\sigma,\tau}{\sigma^3 = 1, \tau^2 = 1, \sigma \tau = \tau \sigma^2},
\]
we have that $\rho_K$ is given by 
\[
  \sigma \mapsto 
  \begin{pmatrix}
    -1 
    &
    1 
    \\ 
    -1 
    & 
    0
  \end{pmatrix},~~
  \tau 
  \mapsto 
  \begin{pmatrix}
    0 
    & 
    1 
    \\
    1 
    & 
    0 
  \end{pmatrix}.
\]
The point now is that \eqref{intro:eq:elliptic} has three distinct solutions if and only if $\tr(\frob_{\ell}) = 2$.
\end{numtext}
